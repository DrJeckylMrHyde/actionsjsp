<%@ page language="java"
         contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Twoje dane</title>
</head>
<body>
<h1>Twoje dane:</h1>

<jsp:useBean id="user" class="pl.javastart.model.User" scope="page">
    <jsp:setProperty name="user" property="firstname" param="firstname" />
    <jsp:setProperty name="user" property="lastname" param="lastname" />
</jsp:useBean>

<p>Imię: <jsp:getProperty property="firstname" name="user"/></p>
<p>Nazwisko: <jsp:getProperty property="lastname" name="user"/></p>
</body>
</html>